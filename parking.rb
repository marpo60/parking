# Provides a class to represent parking allocation.
class Parking
  DEFAULT_START = Time.new 2014, 7, 20

  attr_reader :drivers, :spots, :start, :time

  def initialize(drivers, spots, start = DEFAULT_START, time = Time.now)
    @drivers, @spots, @start, @time = drivers, spots, start, time
  end

  # Returns possible allocations as an array of arrays. If there're more spots
  # than drivers, returns one default allocation.
  def allocations
    combined_allocations || default_allocation
  end

  # Returns allocation for current turn as an array.
  def allocation
    allocations[turn]
  end
  alias_method :turn_allocation, :allocation

  # Returns current turn.
  def turn
    elapsed_weeks % allocations.count
  end

  # Returns elapsed weeks since start.
  def elapsed_weeks
    elapsed_time.div 604800
  end

  private

  def combined_allocations
    drivers.combination(spots).to_a if need_to_combine?
  end

  def default_allocation
    [drivers]
  end

  def need_to_combine?
    spots < drivers.count
  end

  def elapsed_time
    time - start
  end
end

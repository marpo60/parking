require_relative "helper"

class TestStack < Minitest::Test
  include Rack::Test::Methods

  attr_reader :app

  def setup
    @app = Rack::Builder.parse_file(path).first
  end

  def path
    File.expand_path("../../config.ru", __FILE__)
  end

  def test_get
    get "/"
    assert last_response.ok?
  end
end

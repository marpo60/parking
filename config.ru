require "rack"
require_relative "app"

DRIVERS = %w(Adrián Braulio Marcelo)
SPOTS   = 2

use Rack::ConditionalGet
use Rack::ETag
use Rack::Static, urls: %w(/css /fonts /images /js)

run App.new DRIVERS, SPOTS
